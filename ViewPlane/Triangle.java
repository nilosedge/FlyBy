package ViewPlane;

import java.awt.Color;

/** 
* Class used for all triangle operations.
*/
public class Triangle {
	
	/**
	* A worldpoint used for hold the information for the lower left corner of the triangle.
	*/
	public WorldPoint v1;

	/**
	* A worldpoint used for hold the information for the top corner of the triangle.
	*/
	public WorldPoint v2;

	/**
	* A worldpoint used for hold the information for the lower right corner of the triangle.
	*/
	public WorldPoint v3;

	/**
	* A worldpoint used for hold the information for the color of the triangle.
	*/
	public Color c;

	/**
	* Constructor used to create a new triangle for use.
	* @param t is used to make a copy of the incoming Triangle. 
	*/
	public Triangle(Triangle t) {
		this.v1 = t.v1;
		this.v2 = t.v2;
		this.v3 = t.v3;
		this.c = t.c;
	}

	/**
	* Constructor used to create a new triangle for use.
	* @param v1 is used to show the lower left corner of the triangle 
	* @param v2 is used to show the top of the triangle
	* @param v3 is used to show the lower right corner of the triangle
	*/
	public Triangle(WorldPoint v1, WorldPoint v2, WorldPoint v3, Color c) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		this.c = c;
	} 

	/**
	* Function that returns the sum of the Z values.
	*/
	public double sum() {
		return (v1.z + v2.z + v3.z);
	}

}
