package ViewPlane;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
* Class used for reading number from the Model file.
*/
class NumberReader {

	/**
	* BufferedReader used to read in the Model file.
	*/
	protected BufferedReader fin;
	/**
	* Buffer for each line of text.
	*/
	protected String line;  //  Line of input text
	/**
	* Tokenizer used to tokenize the line read in from the file.
	*/
	protected StringTokenizer tokenizer;

	/**
	* Funciton used for reading in a file and tokenizing it.
	* @param reader is for taking in a buffered reader and using it to read files.
	*/
	public NumberReader(BufferedReader reader) {
		fin = reader;
		line = "";
		tokenizer = new StringTokenizer(line);
	}

	/**
	* Funciton used for reading one int value from the line.
	*/
	public int getInt() {
		int result = 0;
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = Integer.valueOf(token).intValue();
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad integer---returning 0");
		}
		return result;
	}
	/**
	* Funciton used for reading one double value from the line.
	*/
	public double getDouble() {
		double result = 0.0;
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = Double.parseDouble(token);
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad double---returning 0");
		}
		return result;
	}
	
	/**
	* Function used to get a string from the line.
	*/
	public String getString() {
		String result = "";
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = token;
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad string token---returning empty string");
		}
		return result;
	}
}
