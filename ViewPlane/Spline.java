package ViewPlane;

import java.io.*;
import java.io.BufferedReader;
import java.util.*;

/**
* Class used for all spline operations. 
*/
public class Spline {

	/**
	* The magic matrix that we learned about in class this is the root of all flying.
	*/
	protected static double[][] splineMatrix
				 = {{ -1.0,  3.0, -3.0, 1.0 },
					 {  3.0, -6.0,  3.0, 0.0 },
					 { -3.0,  0.0,  3.0, 0.0 },
					 {  1.0,  4.0,  1.0, 0.0 }};

	/**
	* Double array used for working with the internal vectors.
	*/
	protected double[] splineResult = new double[4];

	/**
	* Double array used for working with the internal vectors.
	*/
	protected double[] splineVector = new double[4];

	/**
	* The actual array of points that will be used to do the flying.
	*/
	public WorldPoint[] points;

	/**
	* The number of points we have in our curve.
	*/
	public int point_count=0;

	/**
	* Constructor used for loading in a flying curve.
	* @param filename is a string of the filename to load the new curve from.
	*/
	public Spline(String filename) {
		File file = new File(filename);
		if ( !file.exists() || !file.canRead() || file.isDirectory() ) {
			System.out.println("Cannot read " + file);
			return;
		}
		try {
			FileReader fr = new FileReader(file);
			BufferedReader in = new BufferedReader(fr);
			NumberReader fin = new NumberReader(in);
			points = new WorldPoint[fin.getInt()];
			for ( int i = 0; i < points.length;  i++ ) {
				addPoint(fin.getDouble(), fin.getDouble(), fin.getDouble());
			}
		} catch ( Exception e ) {
			System.out.println("**** Error occurred processing file");
		}
	}

	/**
	* Function used to add a new WorldPoint takes 3 double values corispoding to x y z
	* @param x the x axis of the new vertex
	* @param y the y axis of the new vertex
	* @param z the z axis of the new vertex
	*/
	public void addPoint(double x, double y, double z) {
		points[point_count++] = new WorldPoint(x, y, z);
	}

	/**
	* Function used for multiplying a vertor and a matrix together.
	* @param vector is a double array with 4 values in it.
	* @param m is the matrix to multiply the vector by.
	*/
	protected void splineMult(double[] vector, double[][] m) {
		for(int col = 0; col < 4; col++) {
			double sum = 0.0;
			for(int row = 0;  row < 4;  row++) {
				sum += vector[row] * m[row][col];
				splineResult[col] = sum;
			}
		}
		vector[0] = splineResult[0];
		vector[1] = splineResult[1];
		vector[2] = splineResult[2];
		vector[3] = splineResult[3];
	}

	/**
	* Function used for calculateing the value of the point given the T values and the 4 points.
	* @param t is the value at which we want to compute based on p1, p2. p3, p4
	* @param p1 is the first point int he spline
	* @param p2 is the second point int he spline
	* @param p3 is the third point int he spline
	* @param p4 is the fourth point int he spline
	*/
	public double splineFunction(double t, double p1, double p2, double p3, double p4) {
		splineVector[3] = 1.0/6;
		splineVector[2] = splineVector[3]*t;
		splineVector[1] = splineVector[2]*t;
		splineVector[0] = splineVector[1]*t;
		splineMult(splineVector, splineMatrix);
		return splineVector[0]*p1 + splineVector[1]*p2 + splineVector[2]*p3 + splineVector[3]*p4;
	}

	/**
	* Function used to generate the points between all the contorl points for a much smoother or choppy scene.
	*/
	public ArrayList generatePoints(int amt) {
		ArrayList ret = new ArrayList();
		double step_size = ((point_count-3) / (double)amt);
		int counter = 0;
		for(int i = 1; i < point_count - 2; i++) {
			for(double param = 0.0; param <= 1.0; param += step_size) {
				ret.add(counter++, new WorldPoint(
					splineFunction(param, points[i-1].x, points[i].x, points[i+1].x, points[i+2].x),
					splineFunction(param, points[i-1].y, points[i].y, points[i+1].y, points[i+2].y),
					splineFunction(param, points[i-1].z, points[i].z, points[i+1].z, points[i+2].z)
				));
			}
		} return ret;
	}
}
