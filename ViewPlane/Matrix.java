package ViewPlane;

/** 
* Class used for all the Matrix operations.
*/
public class Matrix {

	/**
	* m is a 4 by 4 array used to repersent a 4 by 4 matrix.
	*/
	public double[][] m = new double[4][4];

	/**
	* trans is the transformation matrix used to transform all the points.
	*/
	public Matrix trans;

	/**
	* Constructor used to create a new matrix for use.
	* @param ident if true will make m a identity matrix if false will make a zero filled matrix.
	*/
	public Matrix(boolean ident) {
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				if(ident && i == j) m[i][j] = 1;
				else m[i][j] = 0;
			}
		}
	}

	/**
	* Function used to transform all the Triangles in p.
	* @param p is an array of all the Triangles in the map.
	*/

	public void transformVector(Triangle[] p) {

		for(int i = 0; i < p.length; i++) {
			// I really don't know how the next 44 lines of code
				  // work but its basicly the tranform code
				  // everything seems to be moving around ok so I guess 
				  // it all works
			p[i] = new Triangle(
				new WorldPoint(
					((p[i].v1.x * trans.m[0][0]) +
					(p[i].v1.y * trans.m[1][0]) +
					(p[i].v1.z * trans.m[2][0]) +
					(trans.m[3][0])),
					((p[i].v1.x * trans.m[0][1]) +
					(p[i].v1.y * trans.m[1][1]) +
					(p[i].v1.z * trans.m[2][1]) +
					(trans.m[3][1])),
					((p[i].v1.x * trans.m[0][2]) +
					(p[i].v1.y * trans.m[1][2]) +
					(p[i].v1.z * trans.m[2][2]) +
					(trans.m[3][2]))
				),
				new WorldPoint(
					((p[i].v2.x * trans.m[0][0]) +
					(p[i].v2.y * trans.m[1][0]) +
					(p[i].v2.z * trans.m[2][0]) +
					(trans.m[3][0])),
					((p[i].v2.x * trans.m[0][1]) +
					(p[i].v2.y * trans.m[1][1]) +
					(p[i].v2.z * trans.m[2][1]) +
					(trans.m[3][1])),
					((p[i].v2.x * trans.m[0][2]) +
					(p[i].v2.y * trans.m[1][2]) +
					(p[i].v2.z * trans.m[2][2]) +
					(trans.m[3][2]))
				),
				new WorldPoint(
					((p[i].v3.x * trans.m[0][0]) +
					(p[i].v3.y * trans.m[1][0]) +
					(p[i].v3.z * trans.m[2][0]) +
					(trans.m[3][0])),
					((p[i].v3.x * trans.m[0][1]) +
					(p[i].v3.y * trans.m[1][1]) +
					(p[i].v3.z * trans.m[2][1]) +
					(trans.m[3][1])),
					((p[i].v3.x * trans.m[0][2]) +
					(p[i].v3.y * trans.m[1][2]) +
					(p[i].v3.z * trans.m[2][2]) +
					(trans.m[3][2]))
				),
				p[i].c
			);
		}
	}

	/**
	* Voodoo function that create the transformation matrix.
	* @param vp is the viewplane in which we are looking at.
	*/

	public void vpt(ViewPlane vp) {

		// Preliminary Calculations For Transformation Setup

		WorldPoint vp_norm;
		trans = new Matrix(true);
		double L = Math.sqrt(((vp.norm.x*vp.norm.x) + (vp.norm.y*vp.norm.y) + (vp.norm.z*vp.norm.z)));
		if(L == 0.0) vp_norm = new WorldPoint(0,0,0);
		else vp_norm = new WorldPoint((vp.norm.x*vp.D/L), (vp.norm.y*vp.D/L), (vp.norm.z*vp.D/L));
		double P = Math.sqrt(((vp_norm.y * vp_norm.y) + (vp_norm.z*vp_norm.z)));

		// Reference Point Transformatino

		Matrix T_r = new Matrix(true);
			T_r.m[3][0] = -vp.ref.x;
			T_r.m[3][1] = -vp.ref.y;
			T_r.m[3][2] = -vp.ref.z;
			trans.times(T_r);

		// Normal Point Transformatino

		Matrix T_n = new Matrix(true);
			T_n.m[3][0] = vp_norm.x;
			T_n.m[3][1] = vp_norm.y;
			T_n.m[3][2] = vp_norm.z;
			trans.times(T_n);

		// X Axis Transformation

		Matrix R_x = new Matrix(true);
		if(P == 0) R_x.m[1][1] = R_x.m[2][2] = R_x.m[2][1] = R_x.m[1][2] = 0;
		else {
			R_x.m[1][1] = vp_norm.z/P;
			R_x.m[2][2] = vp_norm.z/P;
			R_x.m[2][1] = -vp_norm.y/P;
			R_x.m[1][2] = vp_norm.y/P;
		} trans.times(R_x);

		// Y Axis Transformation

		Matrix R_y = new Matrix(true);
		if(vp.D == 0) R_y.m[0][0] = R_y.m[0][2] = R_y.m[2][0] = R_y.m[2][2] = 0;
		else {
			R_y.m[0][0] = P/vp.D;
			R_y.m[0][2] = vp_norm.x/vp.D;
			R_y.m[2][0] = -vp_norm.x/vp.D;
			R_y.m[2][2] = P/vp.D;
		} trans.times(R_y);

		// Preliminary Z axis Calculations

		double u_x, u_y;
		if(vp.D == 0) u_x = 0;
		else if(P == 0) u_x = (vp.up.x*(P/vp.D));
		else u_x = (vp.up.x*(P/vp.D) - ((vp_norm.x/(vp.D*P))*(vp.up.y*vp_norm.y + vp.up.z*vp_norm.z)));
		if(P == 0) u_y = 0;
		else u_y = ((1/P)*(vp.up.y*vp_norm.z + vp.up.z*vp_norm.y));
		double W = Math.sqrt(u_x*u_x + u_y*u_y);

		// Z axis Transformation

		Matrix R_z = new Matrix(true);
		if(W == 0) R_z.m[0][0] = R_z.m[0][1] = R_z.m[1][0] = R_z.m[1][1] = 0;
		else {
			R_z.m[0][0] = u_y/W;
			R_z.m[0][1] = u_x/W;
			R_z.m[1][0] = -u_x/W;
			R_z.m[1][1] = u_y/W;
		} trans.times(R_z);

		// Set the new distance 

		vp.D = L;
	}

	/**
	* Function used for multipling this matrix with the incoming matrix.
	* @param m2 is the incoming matrix to multiply this one by.
	*/

	public Matrix times(Matrix m2) {
		Matrix ret = new Matrix(false);
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				for(int k = 0; k < 4; k++) {
					ret.m[i][j] += m[i][k] * m2.m[k][j];
				}
			}
		}
		m = ret.m;
		return ret;
	}

	/**
	* Function used to print this matrix.
	*/

	public void print() {
		System.out.println("[" + m[0][0] + "," + m[0][1] + "," + m[0][2] + "," + m[0][3] + "]");
		System.out.println("[" + m[1][0] + "," + m[1][1] + "," + m[1][2] + "," + m[1][3] + "]");
		System.out.println("[" + m[2][0] + "," + m[2][1] + "," + m[2][2] + "," + m[2][3] + "]");
		System.out.println("[" + m[3][0] + "," + m[3][1] + "," + m[3][2] + "," + m[3][3] + "]\n");
	}

}
