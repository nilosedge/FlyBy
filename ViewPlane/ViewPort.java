package ViewPlane;

import java.awt.*;
import javax.swing.*;

/**
* Class used for actully viewing a map.
*/
public class ViewPort extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1392041543057053322L;

	/**
	* double used for holding the virtual Left point.
	*/
	protected double viewLeft;

	/**
	* double used for holding the virtual Top point.
	*/
	protected double viewTop;

	/**
	* double used for holding the virtual Width point.
	*/
	protected double viewWidth;

	/**
	* double used for holding the virtual Height point.
	*/
	protected double viewHeight;

	/**
	* double used for holding the real Left point.
	*/
	protected int realLeft;

	/**
	* double used for holding the real Top point.
	*/
	protected int realTop;

	/**
	* double used for holding the real Width point.
	*/
	protected int realWidth;

	/**
	* double used for holding the real Height point.
	*/
	protected int realHeight;

	/**
	* The actull map object used for holding the model read in from a file.
	*/
	protected MapLevel map;

	/**
	* Is the view information about where the used is looking at the map.
	*/
	protected ViewPlane viewplane;

	/**
	* Is our working matrix for doing tranformations and such.
	*/
	protected Matrix mat;


	/**
	* ViewPort Creates all the objects used to display the map.<BR><BR>
	* @param left is the Virtual X point of the window that shows up.<BR>
	* @param right is the Virtual Y point of the window that shows up.<BR>
	* @param width is the Virtual Width of the window that shows up.<BR>
	* @param height is the Virtual Height of the window that shows up.<BR>
	* @param rleft is the X value of the topleft point.<BR>
	* @param rtop is the Y value of the topleft point.<BR>
	* @param rwidth is the Width of the window that shows up.<BR> 
	* @param rheight is the Height of the window that shows up.<BR>
	* @param fig is a reference to the map to be displayed.<BR>
	* @param view is a reference to the viewplane information.<BR>
	*/
	public ViewPort(
		double left,
		double top,
		double width,
		double height,
		int rleft,
		int rtop,
		int rwidth,
		int rheight,
		MapLevel fig,
		ViewPlane view) {

		//  Initialize the virtual extent of this viewport
		mat = new Matrix(true);
		viewLeft = left;	viewTop = top;
		viewWidth = width;  viewHeight = height;
		realLeft = rleft; realTop = rtop;
		realWidth = rwidth; realHeight = rheight;
		map = fig;
		viewplane = view;
		setLayout(null);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setBackground(Color.white);
	}

	/**
	* Function actully points the map to the screen.
	*/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// g.translate((int)realWidth/2, (int)realHeight/2);
		mat.vpt(viewplane);
		Triangle[] polys = map.getPolys();

		mat.transformVector(polys);
		perspect(polys);
		polys = kill_backfaces(polys);
		sort(polys);

		for(int i = 0; i < polys.length; i++) {
			if(!(polys[i].v1.z < viewplane.D && polys[i].v2.z < viewplane.D && polys[i].v3.z < viewplane.D)) {
				Polygon p = new Polygon();
				p.addPoint((int)(0.5 + polys[i].v1.x), (int)(0.5 + polys[i].v1.y));
				p.addPoint((int)(0.5 + polys[i].v2.x), (int)(0.5 + polys[i].v2.y));
				p.addPoint((int)(0.5 + polys[i].v3.x), (int)(0.5 + polys[i].v3.y));
				g.setColor(polys[i].c);
				g.fillPolygon(p);
			}
		}
	}

	/**
	* Function returns an array pf polys with the backfaces removed.
	*/
	public Triangle[] kill_backfaces(Triangle[] tri) {
		int counter = 0;
		for(int i = (tri.length - 1); i >= 0; i--) {
			if(!isBackFace(tri[i], viewplane.norm)) {
				counter++;
			}
		}
		Triangle[] out = new Triangle[counter];
		counter = 0;
		for(int i = (tri.length - 1); i >= 0; i--) {
			if(!isBackFace(tri[i], viewplane.norm)) {
				out[counter++] = tri[i];
			}
		}
		return out;
	}

	/**
	* Function that performs the persective projection on that triangles.
	*/
	public void perspect(Triangle[] tri) {
		for(int i = (tri.length - 1); i >= 0; i--) {
			tri[i].v1.x = map_and_perspect(tri[i].v1.x, tri[i].v1.z);
			tri[i].v1.y = map_and_perspect(tri[i].v1.y, tri[i].v1.z);
			tri[i].v1.z = map_and_perspect(tri[i].v1.z, tri[i].v1.z);
			tri[i].v2.x = map_and_perspect(tri[i].v2.x, tri[i].v2.z);
			tri[i].v2.y = map_and_perspect(tri[i].v2.y, tri[i].v2.z);
			tri[i].v2.z = map_and_perspect(tri[i].v2.z, tri[i].v2.z);
			tri[i].v3.x = map_and_perspect(tri[i].v3.x, tri[i].v3.z);
			tri[i].v3.y = map_and_perspect(tri[i].v3.y, tri[i].v3.z);
			tri[i].v3.z = map_and_perspect(tri[i].v3.z, tri[i].v3.z);
		}
	}

	/**
	* Function that check to see if a triangle is a backface or not.
	*/
	public boolean isBackFace(Triangle t, WorldPoint norm) {
		return !(t.v1.x * (t.v2.y - t.v3.y) - t.v1.y * (t.v2.x - t.v3.x) + (t.v2.x * t.v3.y) - (t.v2.y * t.v3.x) > 0.0000001);
	}

	/**
	* Function that sorts the triangles in order of there combined Z values.
	*/
	public void sort(Triangle[] in) {
		for(int i = 0; i < (in.length - 1); i++) {
			int si = i;
			for(int k = i + 1; k < in.length; k++) {
				if(in[k].sum() > in[si].sum()) { si = k; }
			}
			if(i!=si) {
				Triangle temp = in[i];
				in[i] = in[si];
				in[si] = temp;
			}
		}
	}

	/**
	* Maps the real point to the virtual world point, for the x value.
	* @param inx is the x value to be remaped to the virtual world.
	* @param z is the z value used to make the persceptive correct.
	*/
	public double map_and_perspect(double in, double z) {
		double D = 200;
		//double D = (400/viewplane.D);
		in = (D * in / (D + z));
		in *= 4;
		in /= viewplane.D;
		return ((((in-viewLeft)/viewWidth)*(realWidth-realLeft)) + realLeft);
	}
}
