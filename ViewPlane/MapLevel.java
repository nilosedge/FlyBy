//  Figure.java 
//	 A graphical application that allows the user to view wire frame
//	 images.

package ViewPlane;

import java.io.*;
import java.awt.*;
import java.io.BufferedReader;


/**
* A set of Triangles (Polygons) that represents a 3d world.
*/
public class MapLevel {
	
	/**
	* An array of Triangles used to represent the polygon mesh.
	*/
	public Triangle[] polys;
	/**
	* An array of working points that are not the actual points.
	*/
	public Triangle[] polys_working;
	/**
	* The number of polys that are in the set.
	*/
	public int poly_count=0;


	/**
	* Function used to add a new Polys takes 3 World Points corispoding to v1, v2, v3
	* @param v1 the point of the trianlge in the lower left corner. 
	* @param v2 the point of the triangle in the top middle.
	* @param v3 the point of the triangle in the lower right corner.
	*/
	public void addPoly(WorldPoint v1, WorldPoint v2, WorldPoint v3, Color c) {
		polys[poly_count] = new Triangle(v1, v2, v3, c);
		polys_working[poly_count++] = new Triangle(v1, v2, v3, c);
	}

	/**
	* Function used for loading in a new model.
	* @param filename is a string of the filename to load in the new 3d world 
	*/
	public void loadFile(String filename) {
		poly_count=0;

		File file = new File(filename);
		if ( !file.exists() || !file.canRead() || file.isDirectory() ) {
			System.out.println("Cannot read " + file);
			return;
		}

		try {
			FileReader fr = new FileReader(file);
			BufferedReader in = new BufferedReader(fr);
			NumberReader fin = new NumberReader(in);

			int num_polys = fin.getInt();
			polys = new Triangle[num_polys];
			polys_working = new Triangle[num_polys];

			for ( int i = 0; i < num_polys;  i++ ) {
					WorldPoint v1 = new WorldPoint(fin.getDouble(), fin.getDouble(), fin.getDouble());
					WorldPoint v2 = new WorldPoint(fin.getDouble(), fin.getDouble(), fin.getDouble());
					WorldPoint v3 = new WorldPoint(fin.getDouble(), fin.getDouble(), fin.getDouble());
					addPoly(v1, v2, v3, new Color(fin.getInt(),fin.getInt(),fin.getInt()));
			}

		} catch ( Exception e ) {
			System.out.println("**** Error occurred processing file");
		}
	}

	/**
	* Returns a reference to the newly created working polys.
	*/
	public Triangle[] getPolys() { 
		for(int i = 0; i < polys.length; i++) {
			polys_working[i] = new Triangle(polys[i]);
		} return polys_working; 
	}
}

