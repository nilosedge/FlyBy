package ViewPlane;

/**
* Class for holding all the information about the viewer
*/
public class ViewPlane {

	/**
	* A World Point that holds the reference point info.
	*/
	public WorldPoint ref;
	/**
	* A World Point that holds the viewplane normal point info.
	*/
	public WorldPoint norm;
	/**
	* A World Point that holds the up point info.
	*/
	public WorldPoint up;
	/**
	* A World Point that holds the distance to the figure.
	*/
	public double D = -10.0;

	/**
	* Creates a view plane from three world points.
	* @param r a WorldPoint that holds the reference point.
	* @param n a WorldPoint that holds the normal point.
	* @param u a WorldPoint that holds the up point.
	*/
	public ViewPlane(WorldPoint r, WorldPoint n, WorldPoint u) {
		ref = r; norm = n; up = u;
	}

	/**
	* Function used for setting the normal point.
	* @param pt is a reference to the new normal point.
	*/
	public void setNormal(WorldPoint pt) { norm = pt; }

	/**
	* Function used for setting the normal point.
	* @param x is the x value of the new normal point.
	* @param y is the y value of the new normal point.
	* @param z is the z value of the new normal point.
	*/
	public void setNormal(double x, double y, double z) {
		norm.x = x; 
		norm.y = y; 
		norm.z = z;
	}

	/**
	* Function used to print the 3 different points, mostly used for debuging.
	*/
	public void printPoints() {
		System.out.println("Reference Point: (" + ref.x + "," + ref.y + "," + ref.z + ")");
		System.out.println("Normal Point: (" + norm.x + "," + norm.y + "," + norm.z + ")");
		System.out.println("Up Point: (" + up.x + "," + up.y + "," + up.z + ")\n");
	}
}
