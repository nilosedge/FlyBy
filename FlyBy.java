//	FlyBy.java 
//	 A graphical application that allows the user to view wire frame
//	 images.

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import ViewPlane.*;

/**
* Main class for creating a view for wireframe images.
*/
public class FlyBy extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7174461298619795379L;
	/**
	* Virtual Left Value
	*/
	protected double virtualLeft;
	/**
	* Virtual Top Value
	*/
	protected double virtualTop;
	/**
	* Virtual Width Value
	*/
	protected double virtualWidth;
	/**
	* Virtual Height Value
	*/
	protected double virtualHeight;
	/**
	* ViewPort for Viewing the Model 
	*/
	protected ViewPort viewport;
	/**
	* Map Object for holding all the points of the Model. 
	*/
	protected MapLevel map;
	/**
	* ViewPlane that holds the infomation for looking through the viewport. 
	*/
	protected ViewPlane viewplaneInfo;

	/**
	* FlyBy Creates a window and controls that allow you to move around the model.<BR><BR>
	* @param realLeft is the X value of the topleft point.<BR>
	* @param realTop is the Y value of the topleft point.<BR>
	* @param realWidth is the Width of the window that shows up.<BR> 
	* @param realHeight is the Height of the window that shows up.<BR>
	* @param vLeft is the Virtual X point of the window that shows up.<BR>
	* @param vRight is the Virtual Y point of the window that shows up.<BR>
	* @param vWidth is the Virtual Width of the window that shows up.<BR>
	* @param vHeight is the Virtual Height of the window that shows up.<BR>
	*/

	public FlyBy(int realLeft, int realTop, int realWidth, int realHeight, double vLeft, double vTop, double vWidth, double vHeight) {

		virtualLeft = vLeft;

		virtualTop = vTop;
		virtualWidth = vWidth;
		virtualHeight = vHeight;

		setTitle("Fly By Anim");
		setLocation(realLeft, realTop);
		setBackground(Color.black);
		setSize(realWidth, realHeight);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		map = new MapLevel();
		map.loadFile("solids.data");

		viewplaneInfo = new ViewPlane(
			new WorldPoint(0, 0, 0),	// reference point
			new WorldPoint(0, 0, -10), // normal point
			new WorldPoint(0, -1, 0) // up point
		);

		viewport = new ViewPort(
			virtualLeft,
			virtualTop, 
			virtualWidth,
			virtualHeight,
			realLeft,
			realTop,
			realWidth,
			realHeight,
			map,
			viewplaneInfo
		);
		viewport.setBackground(Color.black);

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(viewport, BorderLayout.CENTER);
		setVisible(true);

		Spline spline = new Spline("splines.data");

		ArrayList list = spline.generatePoints(1000);
		int i = 0;

		while(true) {

			for(i = 0; i < list.size(); i++) {
				try { Thread.sleep(10); } 
				catch (InterruptedException e) {}
				viewplaneInfo.norm = (WorldPoint)list.get(i);
				repaint();
			}
			for(i = (list.size() - 1); i >= 0;  i--) {
				try { Thread.sleep(10); } 
				catch (InterruptedException e) {}
				viewplaneInfo.norm = (WorldPoint)list.get(i);
				repaint();
			}
		}

	}

	public static void main(String[] args) {
		FlyBy window = new FlyBy(10, 10, 700, 700, -30.0, -30.0, 60.0, 60.0);
	}

}
